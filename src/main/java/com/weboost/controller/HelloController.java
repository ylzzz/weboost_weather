package com.weboost.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Author:yanlz
 *
 * @Date:12:27 AM 2019/5/12
 */
@RestController
public class HelloController {

    @ResponseBody
    @RequestMapping("/")
    public String index(){
        return "Hello,weBoost!";
    }
}

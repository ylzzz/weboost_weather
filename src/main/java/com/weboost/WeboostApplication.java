package com.weboost;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WeboostApplication {

    public static void main(String[] args) {
        SpringApplication.run(WeboostApplication.class, args);
    }

}
